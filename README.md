# timeli-wavetronix-redisdb
The purpose of this application is to feed in necessary information into an instance of Redis Database to help [wavetronix-sparkstreaming](https://bitbucket.org/rbyna/timeli-wavetronics-sparkstreaming/src/master/) in detecting potential incidents. It uploads the following data as key-value pairs into redis :

1. Threshold data of detectors 
2. Target Detectors
3. Detector Data
4. Detector and its neighbors within 2 miles

Additional, information about the data stored can be found here [wavetronix-redis-data](https://docs.google.com/document/d/1ga7ZQJGntJydvXP_YrrddWORDEQoEAUkoExVzghH5M4/edit?ts=5cf96c67#heading=h.uybfvoxvv8ev).

## Prerequisites
1. [Redis](https://redis.io/) 
    * version : 4.0.11
    * Configuration file location : `/etc/redis/redis.conf`
    * To start redis server : `sudo systemctl start redis`
    * To stop redis server : `sudo systemctl stop redis`
2. [Java](https://www.java.com/en/) 
     * version : 1.8
3. [Maven](https://maven.apache.org/)   

## Steps to run the application :
1. Make sure redis server is running
2. Use `maven` to build the jar `maven install` 
3. `java -jar timeli-wavetronix-redis.jar` Run it as simple java application

## Verification
To verify the data in redis, first connect to redis using redis-cli on the machine it is installed on. Based on the redis configuration, password maybe required to perform queries. 

1. Threshold Data : Use HGETALL to get the threshold data. For example,
     * `HGETALL "CBDS 14 EB:2:6"`  should return something like this `1) "2.0" 2) "64.875" 3) "0.0" 4) "65.57356262207031" 5) "1.0" 6) "65.34558868408203" 7) "3.0" 8) "64.66710662841797"`
2. Target Detector : Use HGET
       * `HGET "I-35 NB @ County Rd E57" detectorId` should return true if the detector is in the target detector file.  
3. Segment Data : Use HGETALL. For example,
       * `HGETALL "CBDS 14 EB"` should return all the information(fields) about the segment. 
    * `HGET "CBDS 14 EB" station_Id` should return the station id of the detector.
4. Near by segments : Use lrange.
    * `lrange "CBDS 14 EBL" 0 100` should print all the first 100 near by detectors of `CBDS 14 EB`.

Sample files of all the inputs are present in resource folder.
  
## Known Issues/Bugs
1. Uploads near by detectors of all the detectors instead of only target detectors.
2. Logic of finding near by detector function needs to be verified. 
3. Current logic of the application doesn't provide the safety from uploading data into redis multiple times.

    * For example, running this application the second time without clearing the data in redis will add duplicates into the near by detectors list. So, make sure to clear wavetronix data in redis before running this application. 