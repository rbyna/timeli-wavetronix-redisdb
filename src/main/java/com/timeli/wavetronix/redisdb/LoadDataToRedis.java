package com.timeli.wavetronix.redisdb;

import redis.clients.jedis.GeoCoordinate;
import redis.clients.jedis.GeoRadiusResponse;
import redis.clients.jedis.GeoUnit;
import redis.clients.jedis.Jedis;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class LoadDataToRedis {
	
	private ExternalConfig externalConfig;

	
	public LoadDataToRedis(ExternalConfig externalConfig) {
		this.externalConfig = externalConfig;

	}

	/**
	 * Load threshold data.
	 *
	 * @param jedis the jedis
	 * @throws IOException Signals that an I/O exception has occurred.
	 * 
	 * Reads the Threshold data file and add the threshold as a key value pair in redis
	 * 
	 */
	public void loadDetectorThresholdData(Jedis jedis) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(externalConfig.getThresholdDataFileName()));
		// br.readLine(); No header in the file 
		String line = "";

		while ((line = br.readLine()) != null) {
			String[] parts = line.split(",");

			String detectorId = parts[0];
			String weekday = parts[1];
			String hour = parts[2];
			String period = String.valueOf(Double.valueOf(parts[3])); // periods are like 0.0, 1.0, 2.0, 3.0
			Double median = Double.parseDouble(parts[4]);
			Double iqd = Double.parseDouble(parts[5]);

			String key = detectorId + ":" + weekday + ":" + hour;
			double threshold = median - (2 * iqd);
			jedis.hset(key, period, String.valueOf(threshold));
		}
	}

	/**
	 * Load detector information.
	 *
	 * @param jedis the jedis
	 * @throws IOException Signals that an I/O exception has occurred.
	 * Adds all the information of detector into redis (detectorid, field name, field value)
	 */
	public void loadDetectorInformation(Jedis jedis) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(externalConfig.getLocationDataFileName()));
		// skip the header
		br.readLine();
		String line = null;
		while ((line = br.readLine()) != null) {
			// file is csv so split by comma (",")
			// https://stackoverflow.com/questions/1757065/java-splitting-a-comma-separated-string-but-ignoring-commas-in-quotes
			String[] splits = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
			String organization_Id = splits[0];
            String network_Id = splits[1];
			String local_date = splits[2];
			String local_time = splits[3];
			String utc_offset = splits[4];
			String detector_Id = splits[5];
			if (detector_Id.equals("PEEK TEST")) continue;
			String station_Id = splits[6];
			String detector_name = splits[7];
			double latitude = Double.parseDouble(splits[8]) * (Math.pow(10, (-6)));
			double longitude = Double.parseDouble(splits[9]) * (Math.pow(10, (-6)));
			String latitudeStr = String.valueOf(latitude);
			String longitudeStr = String.valueOf(longitude);
			String link_ownership = splits[10];
			String route_designator = splits[11];
			String linear_reference = splits[12];
			String detector_type = splits[13];
			String approach_direction = splits[14];
			String approach_name = splits[15];
			String lanes_type = splits[16];
			String lane_id = splits[17];
			String lane_name = splits[18];


			jedis.hset(detector_Id, "organization_Id", organization_Id);
			jedis.hset(detector_Id, "network_Id", network_Id);
			jedis.hset(detector_Id, "local_date", local_date);
			jedis.hset(detector_Id, "local_time", local_time);
			jedis.hset(detector_Id, "utc_offset", utc_offset);
			jedis.hset(detector_Id, "station_Id", station_Id);
			jedis.hset(detector_Id, "detector_name", detector_name);
			jedis.hset(detector_Id, "latitude", latitudeStr);
			jedis.hset(detector_Id, "longitude", longitudeStr);
			jedis.hset(detector_Id, "link_ownership", link_ownership);
			jedis.hset(detector_Id, "route_designator", route_designator);
			jedis.hset(detector_Id, "linear_reference", linear_reference);
			jedis.hset(detector_Id, "detector_type", detector_type);
			jedis.hset(detector_Id, "approach_direction", approach_direction);
			jedis.hset(detector_Id, "approach_name", approach_name);
			jedis.hset(detector_Id, "lanes_type", lanes_type);
			jedis.hset(detector_Id, "lane_id", lane_id);
			jedis.hset(detector_Id, "lane_name", lane_name);
		}
		br.close();
	}

	/**
	 * Adds a new field detectorId as true (detector_id, "detectorId", true)
	 */
	public void loadTargetDetectors(Jedis jedis) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(externalConfig.getTargetDetectorsFileName()));

		// skip the header
		// br.readLine();

		String line = null;

		while ((line = br.readLine()) != null) {
			jedis.hset(line.trim(), "detectorId", "true");
		}
		br.close();
	}

	/**
	 * Load segment geo data.
	 *
	 * @param jedis the jedis
	 * @throws IOException Signals that an I/O exception has occurred.
	 * Reads all the detectors from detector data file and adds detectorId and its near by segments list as key value pair
	 * (detectorId + L, list of segments) 
	 */
	public void loadTargetNearByDetectorsList(Jedis jedis) throws IOException {

		BufferedReader br = new BufferedReader(new FileReader(externalConfig.getLocationDataFileName()));

		// skip the header
		br.readLine();

		String line = null;

		HashMap<String, HashSet<String>> roadNameToDetectorList = new HashMap<>();
		
		
		
		while ((line = br.readLine()) != null) {
            String[] splits = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
            String detector_Id = splits[5].trim();
            String route_designator = splits[11].trim();
            /*
			 * Creates a hashmap with road name as key and list of all detector ids it is associated with 
			 */
				if (roadNameToDetectorList.get(route_designator) == null) {
					HashSet<String> detectorList = new HashSet<>();
					detectorList.add(detector_Id);
					roadNameToDetectorList.put(route_designator, detectorList);
				} else {
					HashSet<String> detectorList = roadNameToDetectorList.get(route_designator);
					detectorList.add(detector_Id);
					roadNameToDetectorList.put(route_designator, detectorList);
				}
			}
		/*
		 * Adds geo data details into redis with detector id as key and hashmap of detector id and its geo coordinates
		 */
		for (String roadname : roadNameToDetectorList.keySet()) {

			HashSet<String> detectorList = roadNameToDetectorList.get(roadname);

			Map<String, GeoCoordinate> coordinateMap = new HashMap<String, GeoCoordinate>();
			for (String detectorId : detectorList) {
				String latitudeStr = jedis.hget(detectorId, "latitude");
				String longitudeStr = jedis.hget(detectorId, "longitude");

				if (latitudeStr == null || latitudeStr.isEmpty() || longitudeStr == null || longitudeStr.isEmpty()) continue;
				double latitude = Double.parseDouble(latitudeStr)* (Math.pow(10, (-6)));
				double longitude = Double.parseDouble(longitudeStr)* (Math.pow(10, (-6)));
				coordinateMap.put(detectorId, new GeoCoordinate(longitude, latitude));
			}
			jedis.geoadd(roadname, coordinateMap);
		}
		
		
		/*
		 * Adds neighbors of a detectors within 2 miles using approach_direction. Key will be code+L and value will be 
		 * list of detector ids 
		 */
		for(String roadname : roadNameToDetectorList.keySet()) {
			HashSet<String> detectorList = roadNameToDetectorList.get(roadname);
			
			for(String detectorId : detectorList) {
				String latitudeStr = jedis.hget(detectorId, "latitude");
				String longitudeStr = jedis.hget(detectorId, "longitude");

				if (latitudeStr == null || latitudeStr.isEmpty() || longitudeStr == null || longitudeStr.isEmpty()) continue;
				double latitude = Double.parseDouble(latitudeStr)* (Math.pow(10, (-6)));
				double longitude = Double.parseDouble(longitudeStr)* (Math.pow(10, (-6)));
				String bearing = jedis.hget(detectorId, "approach_direction");
				
				List<GeoRadiusResponse> members = jedis.georadius(roadname, longitude, latitude, 2, GeoUnit.MI);
				
				int size = members.size();
				
				for(int i = 0; i < size; i++) {
					String detectorWithinDistance = members.get(i).getMemberByString();
					
					if(!detectorWithinDistance.equals(detectorId)) {
						String newbearing = jedis.hget(detectorWithinDistance, "approach_direction");
						if(bearing.equals(newbearing)) {
//							System.out.println(code + " " + codeWithinDistance);
							jedis.lpush(detectorId + "L", detectorWithinDistance);
						}
					}
				}
			}
			
		}
	}

}
